﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

static public class CONSTANTS
{
    public const float DEFAULT_ATK = (float)0.0;
    public const float DEFAULT_DEF = (float)0.0;
    public const float EVADE_ATK = (float)-100.0;
    public const float EVADE_DEF = (float)40.0;
    public const float ATTACKMOVE_ATK = (float)-10.0;
    public const float ATTACKMOVE_DEF = (float)20.0;
    public const float FOCUS_ATK = (float)50.0;
    public const float FOCUS_DEF = (float)-60.0;
    public const float MILLI = (float)1000.0;
    public const float STEPS_FLEET = 1 /4f;
    public const float STEPS_SHOT = 1f;
    public const float SHOT_SCALE = 0.35f;
    public const float FLEET_SCALE = 2f;
}


public class World : MonoBehaviour
{
    /// <summary>
    /// the class World connects between graphics to logic.
    /// the rest of the classes DO NOT include any graphics in them.
    /// </summary>

    public GameObject prefab = null;

    List<GameObject> fleets = new List<GameObject>();

    public GameObject CreateFleet()
    {
        GameObject g = new GameObject("Fleet");
        return g;
    }

    // the GameObject is the type that has all the properties of a object inside the game
    public GameObject CreateSpaceship()
    {
        GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Spaceship s = g.AddComponent<Spaceship>();
        s.Init(new Vector3(0, 0, 0), "AG450", 120, new List<Weapon>());
        return g;
    }

    public void MoveFleet()
    {

    }

    // Use this for initialization
    void Start()
    {
        GameObject sphere;
        if (Network.isClient)
        {
            for (int i = 0; i < 5; i++) // Fleet1
            {
                sphere = Network.Instantiate(prefab, new Vector3(10, 0, 10 + (i + 1) * 8), Quaternion.identity, 0) as GameObject;
                Rigidbody r = sphere.GetComponent<Rigidbody>();
                sphere.transform.localScale = new Vector3(CONSTANTS.FLEET_SCALE, CONSTANTS.FLEET_SCALE, CONSTANTS.FLEET_SCALE);
                sphere.GetComponent<Renderer>().material.color = Color.red;
                r.useGravity = false;
                r.isKinematic = true;
                sphere.AddComponent<Fleet>();
                sphere.GetComponent<Fleet>().Init(sphere.transform.position);
                sphere.GetComponent<Fleet>().SetDestination(sphere.transform.position);
                sphere.tag = "Teem1";
            }
        }
        if (Network.isServer)
        {
            for (int i = 0; i < 5; i++) // Fleet2
            {
                sphere = Network.Instantiate(prefab, new Vector3(-10, 0, 10 + (i + 1) * 8), Quaternion.identity, 0) as GameObject;
                Rigidbody r = sphere.GetComponent<Rigidbody>();
                sphere.transform.localScale = new Vector3(2f, 2f, 2f);
                sphere.GetComponent<Renderer>().material.color = Color.yellow;
                r.useGravity = false;
                r.isKinematic = true;
                sphere.AddComponent<Fleet>();
                sphere.GetComponent<Fleet>().Init(sphere.transform.position);
                sphere.GetComponent<Fleet>().SetDestination(sphere.transform.position);
                sphere.tag = "Teem2";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}

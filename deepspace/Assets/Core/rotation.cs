﻿using UnityEngine;
using System.Collections;

public class rotation : MonoBehaviour {

    public float minX = -360;
    public float maxX = 360;

    public float minY = -45;
    public float maxY = 45;

    public float xSpeed = 100;
    public float ySpeed = 100;

    float rotationY = 0;
    float rotationX = 0;

    private float zoomSpeed = 2;
    private float speed = 5;

    // Use this for initialization
    void Start () {
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            rotationX += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime;
            rotationY += Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime;
            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        ////// scorlling not good yet
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        transform.Translate(0, -scroll * zoomSpeed * 0.5f, scroll * zoomSpeed, Space.Self);


        if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
        {
            float h = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
            Vector3 RIGHT = transform.TransformDirection(Vector3.right);
            transform.localPosition += RIGHT * h;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            float h = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
            Vector3 RIGHT = transform.TransformDirection(Vector3.right);
            transform.localPosition += RIGHT * h;
        }
        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow))
        {
            float v = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            Vector3 FORWARD = transform.TransformDirection(Vector3.forward);
            transform.localPosition += FORWARD * v;
        }
        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.UpArrow))
        {
            float v = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            Vector3 FORWARD = transform.TransformDirection(Vector3.forward);
            transform.localPosition += FORWARD * v;
        }
    }

    /*void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Vector3 pos =this.GetComponent<Transform>().position;
        if (stream.isWriting)
        {
            // Sending

            stream.Serialize(ref pos);
        }
        else {
            // Receiving
            stream.Serialize(ref pos);
            this.GetComponent<Transform>().position = pos;
            // ... do something meaningful with the received variable
        }
    }*/
}

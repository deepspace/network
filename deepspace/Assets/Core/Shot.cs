﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour
{
    // prog fields
    private Vector3 location;
    private Vector3 destination;
    private Fleet target;
    private bool isGuided;
    private Vector3 velocity;
    private int border = 1000;

    public void Init(Vector3 location, Fleet target, bool isGuided, Vector3 velocity)
    {
        this.location = location;
        this.target = target;
        this.destination = target.transform.position;
        this.isGuided = isGuided;
        this.velocity = velocity;
    }

    // move a step.
    public void Advance()
    {
        if (isGuided)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, CONSTANTS.STEPS_SHOT);
        }
        else
            this.transform.position += (this.destination - this.location) / (float)(this.destination - this.location).magnitude * CONSTANTS.STEPS_SHOT;
    }

    // making the game object.
    public enum ShotsType
    {
        
    };

    // check collision
    void OnCollisionEnter(Collision col)
    {
        Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
        Rigidbody body = this.gameObject.GetComponent<Rigidbody>();
        body.useGravity = false;
        body.isKinematic = false;
        this.transform.localScale = new Vector3(CONSTANTS.SHOT_SCALE, CONSTANTS.SHOT_SCALE, CONSTANTS.SHOT_SCALE);
        this.gameObject.transform.position = this.location;
    }

    void update()
    {
        if (transform.position.x > border || transform.position.x < -border || transform.position.y > border || transform.position.y < -border || transform.position.z > border || transform.position.z < -border)
            Object.Destroy(this.gameObject);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        this.Advance();
	}
}

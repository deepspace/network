﻿using UnityEngine;
using System.Collections.Generic;

public class Spaceship : MonoBehaviour
{
    public enum Behaviour
    {
        Default, Evade, AttackMove, Focus
    };
    // Behaviour System Explaination //
    // bonus and penalty for the ship depending on its behaviour.
    // the behaviour system allow for giving each spaceship its own behaviour, besides the fleet orders.
    // <status>: <attack bonus/penalty>% <defence bonus/penalty>%
    // Default:      0%      0%  (spaceship is hovering and attacking, modifiers are not changed)
    // Evade:     -100%    +40%  (spaceship doing extreme manuevers, can not attack)
    // AttackMove: -10%    +20%  (spaceship is moving and shooting, harder to hit hostiles, harder for hostiles to hit)
    // Focus:      +50%    -50%  (spaceship is steady aiming, for perfect shots, much better aim, vulnurable to attacks)

    private Behaviour behaviour;
    private int health;
    private List<Weapon> weapons;
    private List<Shot> incomingShots;


    public Spaceship(Vector3 location, string name, int health, List<Weapon> weapons)
    {
        this.Init(location, name, health, weapons);
    }

    // initializing object from the outside World class
    public void Init(Vector3 location, string name, int health, List<Weapon> weapons)
    {
        this.gameObject.transform.position = location;
        this.name = name;
        this.health = health;
        this.weapons = new List<Weapon>(weapons);
        this.behaviour = Behaviour.Default;
    }

    public Spaceship(Spaceship other)
    {
        this.Init(other.Location, other.name, other.health, other.weapons);
    }

    // target another ship. for now, all of the weapons on the attacking ship attack the target
    public void Attack(Spaceship target)
    {

    }

    // checks wether any weapon on the ship is reloaded
    public bool IsAnyMountedWeaponReloaded()
    {
        bool ans = false;
        for (int i = 0; i < this.weapons.Count; i++)
        {
            if (this.weapons[i].IsReloaded())
            {
                ans = true;
            }
        }
        return ans;
    }

    // alert about being fired by a mounted weapon
    private void FiredUpon(Shot shot)
    {
        this.incomingShots.Add(shot);
    }

    // get hit by a ship-mounted-weapon
    private void GetHit(Weapon weapon)
    {
        this.health -= weapon.Damage;
    }

    // easier function to change behaviour
    public void SetBehaviour(Behaviour b)
    {
        this.behaviour = b;
    }

    // the functions below change the status of the spaceship.
    public void Default()
    {
        this.behaviour = Behaviour.Default;
    }
    public void Evade()
    {
        this.behaviour = Behaviour.Evade;
    }
    public void AttackMove()
    {
        this.behaviour = Behaviour.AttackMove;
    }
    public void Focus()
    {
        this.behaviour = Behaviour.Focus;
    }

    // these functions return the attack or defence modifiers of the status
    static public float GetAttackModifier(Behaviour behaviour)
    {
        switch (behaviour)
        {
            case (Behaviour.Evade):
                return CONSTANTS.EVADE_ATK;
            case (Behaviour.AttackMove):
                return CONSTANTS.ATTACKMOVE_ATK;
            case (Behaviour.Focus):
                return CONSTANTS.FOCUS_ATK;
            default:
                return CONSTANTS.DEFAULT_ATK;
        }
    }
    static public float GetDefenceModifier(Behaviour behaviour)
    {
        switch (behaviour)
        {
            case (Behaviour.Evade):
                return CONSTANTS.EVADE_DEF;
            case (Behaviour.AttackMove):
                return CONSTANTS.ATTACKMOVE_DEF;
            case (Behaviour.Focus):
                return CONSTANTS.FOCUS_DEF;
            default:
                return CONSTANTS.DEFAULT_DEF;
        }
    }

    // Getters
    public Behaviour ShipBehaviour
    {
        get
        {
            return this.behaviour;
        }
    }
    public int Health
    {
        get
        {
            return this.health;
        }
    }
    public bool IsAttacked
    {
        get
        {
            return this.incomingShots.Count != 0;
        }
    }
    public Vector3 Location
    {
        get
        {
            return this.gameObject.transform.position;
        }
    }


    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        
    }
}

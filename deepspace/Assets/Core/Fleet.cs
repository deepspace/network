﻿using UnityEngine;
using System.Collections.Generic;

public class Fleet : MonoBehaviour
{
    private static int count = 0;
    private int id;
    private List<Spaceship> ships;
    //private Tactic tactic;
    private Vector3 location;
    private Vector3 destination;
    
    // Initializes values
    public void Init(Vector3 location)
    {
        this.location = location;
        this.ships = new List<Spaceship>();
    }

    public Fleet(Vector3 location)
    {
        Init(location);
    }

    // add ships to the Fleet
    public void AddShips(List<Spaceship> shipsToAdd)
    {
        for (int i = 0; i < shipsToAdd.Count; i++)
        {
            this.AddShip(shipsToAdd[i]);
        }
    }

    // add a single ship to the fleet
    public void AddShip(Spaceship shipToAdd)
    {
        this.ships.Add(shipToAdd);
    }

    // set next destination.
    public void SetDestination(Vector3 nextDestination)
    {
        this.destination = nextDestination;
    }

    // make one step.
    public void Move()
    {
        // the game is split into virtual squares.
        // each virtual-time-interval change, all the fleets are allowed to make one step towards the destination.
        this.location=Vector3.MoveTowards(this.location, this.destination, CONSTANTS.STEPS_FLEET); // moves 1/4f steps torwards the destination
    }

    // split the fleet into 2 fleets, with parameter indicating how many ships remain in the current fleet
    public Fleet Split(int ships_count)
    {
        Fleet new_fleet = null;
        if (ships_count < this.Size)
        {
            new_fleet = new Fleet(this.location);
            List<Spaceship> ships = new List<Spaceship>();
            ships.AddRange(this.ships.GetRange(0, ships_count));
        }
        return new_fleet;
    }

    // check if the fleets are close enough to engage each other
    public bool CanEngage(Fleet other)
    {
        bool ans = false;
        double distance = Vector3.Distance(other.location, this.location);
        if (distance <= Fleet.max_engagement_distance)
        {
            ans = true;
        }
        return ans;
    }

    // starts an engagement with a hostile fleet if possible
    public void Engage(Fleet target)
    {
        ParseTactic(target);
        target.UpdateFleet();
    }

    // 
    private void UpdateFleet()
    {
        for (int i = 0; i < this.ships.Count; i++)
        {
            if (this.ships[i].Health <= 0)
            {
                this.ships.Remove(this.ships[i]);
            }
        }
    }

    // parse tactic
    private void ParseTactic(Fleet enemyFleet)
    {
        Spaceship[] enemyShips = enemyFleet.ships.ToArray(); // easier access to the hostile fleet ships
        Spaceship[] friendlyShips = this.ships.ToArray();
        Spaceship ship;
        for (int i = 0; i < friendlyShips.Length; i++)
        {
            ship = friendlyShips[i];

            if (i % 2 == 0)
            {
                ship.SetBehaviour(Spaceship.Behaviour.Focus);
            }
            else
            {
                ship.SetBehaviour(Spaceship.Behaviour.AttackMove);
            }
            ship.Attack(enemyShips[0]);

        }
    }

    // Getters
    public Vector3 Location
    {
        get
        {
            return this.location;
        }
    }
    public Vector3 Destination
    {
        get
        {
            return this.destination;
        }
    }
    public int Size
    {
        get
        {
            return this.ships.Count;
        }
    }

    // the maximum engagement distance for two fleets.
    static public double max_engagement_distance = 10;
    // the maximum distance a fleet does in one step(Maximum velocity).
    static public double max_valocity = 1;

    // Use this for initialization
    void Start ()
    {
	
	}

    // Update is called once per frame
    void Update()
    {
       
    }
}

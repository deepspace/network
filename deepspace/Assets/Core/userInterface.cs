﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class userInterface : MonoBehaviour {

    private int clickedNum;
    private RaycastHit clickedOn;
    private Vector3 screenPoint;
    private Vector2 mousePosition;
    private bool pressed = false;
    private List<GameObject> fleetsInMove; // will hold all of the fleets that in move.
    
    // Use this for initialization
    void Start () {
        clickedNum = 0;
        clickedOn = new RaycastHit();
        fleetsInMove = new List<GameObject>();
    }
    
    // Update is called once per frame
    void Update () {
        if (pressed == false)
        {
            if (Input.GetMouseButtonDown(0) && UnityEngine.Camera.FindObjectOfType<Camera>() != null && clickedNum == 0)
                objectPressed(); // object is pressed
            else if (Input.GetMouseButtonDown(0) && clickedNum == 1)
                moveFleet();  // the user wants to move the object.
            else if (Input.GetMouseButtonDown(0) && clickedNum == 2)
                attackFleet(); // the user wants to attack another fleet.
            else if (clickedNum == 3)
                splitFleet();  // the user wants to split the fleet.
        }
        for (int i = 0; i < fleetsInMove.Count; i++)
        {
            fleetsInMove[i].GetComponent<Fleet>().Move(); // move in the logic.
            fleetsInMove[i].transform.position = Vector3.MoveTowards(fleetsInMove[i].transform.position, fleetsInMove[i].GetComponent<Fleet>().Destination, CONSTANTS.STEPS_FLEET); // move in the graphic.
            if (Vector3.Distance(fleetsInMove[i].GetComponent<Fleet>().Destination, fleetsInMove[i].transform.position) < 0.1) // delete the fleet that has arived the destination.
            {
                fleetsInMove.RemoveAt(i);
                i--;
            }
        }
    }

    void OnGUI()
    {
        if (pressed) // need to show the menu
        {
            GUIStyle customButton = new GUIStyle("button");
            string[] actions = new string[] { "Move!", "Attack!", "Split!" };
            customButton.fontSize = 16;
            for (int i = 1; i <= actions.Length; i++)
            {
                if (GUI.Button(new Rect(mousePosition.x, mousePosition.y+27*(i-1), 80, 25), actions[i-1], customButton))
                {
                    pressed = false;
                    clickedNum = i;
                }
            }
        }
    }

    private void objectPressed()
    {
        Ray ray = UnityEngine.Camera.FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) // hit = object that was pressed.
        {
            clickedOn = hit;
            screenPoint = Camera.main.WorldToScreenPoint(clickedOn.transform.position);
            mousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
            pressed = true;
        }
    }

    private void moveFleet()
    {
        clickedNum = 0;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        if (clickedOn.transform.gameObject.GetComponent<Fleet>()) // if the object that was pressed is a fleet
        {
            clickedOn.transform.gameObject.GetComponent<Fleet>().SetDestination(curPosition);
            fleetsInMove.Add(clickedOn.transform.gameObject);
        }
        else
            Debug.Log("You should click on a fleet!");
    }

    private void attackFleet()
    {
        Ray ray = UnityEngine.Camera.FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) // hit = the object that was pressed.
        {
            if ((hit.transform.gameObject.tag != "Teem1" && clickedOn.transform.gameObject.tag == "Teem1") ||
                (hit.transform.gameObject.tag != "Teem2" && clickedOn.transform.gameObject.tag == "Teem2")) // if you try to attack the other teem
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.AddComponent<Rigidbody>();
                Vector3 direction = hit.transform.position - clickedOn.transform.position;
                sphere.transform.position = clickedOn.transform.position + new Vector3(CONSTANTS.SHOT_SCALE, CONSTANTS.SHOT_SCALE, CONSTANTS.SHOT_SCALE) + CONSTANTS.FLEET_SCALE * (new Vector3(direction.x>0?1:direction.x==0?0:-1, direction.y>0?1: direction.y ==0?0:- 1,direction.z>0?1: direction.z==0?0:- 1));
                sphere.AddComponent<Shot>().Init(sphere.transform.position, hit.transform.gameObject.GetComponent<Fleet>(), true, new Vector3(0,0,0));
            }
            else
                Debug.Log("You can only attack a fleet from the other teem!");
        }
        clickedNum = 0;
    }

    private void splitFleet() // to do
    {
        Debug.Log("Need to split!");
        clickedNum = 0;
    }
}

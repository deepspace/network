﻿using UnityEngine;
using System.Timers;

public class Weapon : MonoBehaviour
{
    private int accuracy;
    private int damage;
    private Timer reloader;
    private float range;
    private bool isGuided;
    private Vector3 shotsVelocity;

    public Weapon(string name, float reloadTime, int accuracy, int damage, float range, bool isGuided, Vector3 velocity)
    {
        this.name = name;
        this.accuracy = accuracy;
        this.damage = damage;
        this.range = range;
        this.isGuided = isGuided;
        this.shotsVelocity = velocity;
        this.reloader = new Timer(reloadTime * CONSTANTS.MILLI);
        // set the timer to work properly with 'delegate'
        this.reloader.Elapsed += delegate (object o, ElapsedEventArgs e)
        {
            this.reloader.Stop();
        };
    }

    // fire the weapon.
    public Shot Fire(Vector3 location)
    {
        Shot ret = null;
        if (this.CanFire())
        {
            this.reloader.Start();
        }
        return ret;
    }

    // checks wheter the weapon can fire.
    public bool CanFire()
    {
        bool ans = false;
        if (this.IsReloaded())
        {
            ans = true;
        }
        return ans;
    }

    // checks if the weapon is ready to be used again
    public bool IsReloaded()
    {
        return this.reloader.Enabled == false;
    }

    // Getters
    public int Damage
    {
        get
        {
            return this.damage;
        }
    }
    public string Name
    {
        get
        {
            return this.name;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class message {

    private string CODE; // will hold the code: "CreateRoom","DeleteRoom", ...
    private List<string> parameters=null;

    public message(string msg)
    {
        if (this.isValid(msg))
        { 
            this.CODE = getCodeFromMSG(msg);
            this.parameters = getParametersFromMSG(msg);
        }
        else
        {
            this.CODE = "Syntax error";
            this.parameters = null;
        }
    }
    private string getCodeFromMSG(string msg)
    {
        return msg.Split('|')[0];
    }
    private List<string> getParametersFromMSG(string msg)
    {
        string[] all = msg.Split('|');
        List<string> onlyParameters = new List<string>();
        for (int i=0;i< all.Length - 3; i++) // for example "CreateRoom|room1||" after split -> ["CreateRoom","room1","",""]
        {
            onlyParameters.Add(all[i + 1]);
        }
        return onlyParameters;
    }

    private bool isValid(string msg)
    {
        bool returnValue = true;
        //TO DO: a lot...
        return returnValue;
    }
    public string getCode()
    {
        return this.CODE;
    }
    public List<string> getParameters()
    {
        return this.parameters;
    }
}

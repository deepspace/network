﻿using UnityEngine;
using System.Collections;

public class color : MonoBehaviour {

    Transform t;
    float h;
    float w;
    int imageX = 3;
    int imageZ = 3;
    // Use this for initialization
    void Start () {
        t = this.gameObject.GetComponent<Transform>();
        Resize();
        h = Screen.height;
        w = Screen.width;
        //this.gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        //t.localScale = new Vector3(Screen.width/40, 1, Screen.height/40);
        if (h != Screen.height || w != Screen.width)
        {
            Resize();
            h = Screen.height;
            w = Screen.width;
        }

    }
    private void Resize()
    {
        float worldScreenHeight;
        float worldScreenWidth;
        if (Screen.height/imageZ>Screen.width/imageX)
        {
            worldScreenWidth = Camera.main.orthographicSize * 2;
            worldScreenHeight = worldScreenWidth / Screen.width * Screen.height;
        }
        else
        {
            worldScreenHeight = Camera.main.orthographicSize * 2;
            worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        }
         
        transform.localScale = new Vector3(worldScreenWidth / imageX, 1,worldScreenHeight / imageZ);
    }
}
